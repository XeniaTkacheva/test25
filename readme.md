##Тестовое задание

Создать новый репозиторий на Gitlab.com. 
В качестве первого коммита запушить базовую сборку php фреймворка Laravel.

Вторым коммитом накатить базовую сборку backpack (https://backpackforlaravel.com)
Админку нужно кастомизировать в соответствии с чеклистом
Третьим коммитом выполнить задание

###Админка
Админку делаем средствами backpackforlaravel

В админке 2 раздела:
Справочники
Автомобили

Раздел Справочники при нажатии раскрыватся :
Марки
Модели

Марки - список, добавление, редактирование, удаление. Каждая сущность: name (varchar64)
Модели - список, добавление, редактирование, удаление. Каждая сущность: name (varchar64) + связь с 1 маркой. 

Удаление моделей реализуем софтделитом. Удаление марок без софтделитов. При удалении марки удаляем все модели, связанные с маркой.

Раздел Автомобили - список, добавление, редактирование, удаление.
В списке надо сделать фильтрацию по маркам (https://backpackforlaravel.com/docs/3.4/crud-filters). Фильтр должен сохраняться при переходах между разделами.

Каждая сущность:
Марка
Модель (выбор доступен после выбора марки только по моделям этой марки)
Загрузка 1 фото (используем кроппер https://backpackforlaravel.com/docs/3.4/crud-fields#image, пропорции 4х3, фото уменьшаем до 1000px, не увеличиваем). Предусмотреть удаление загруженной фото. Фото храним файлами (не в бд), при удалении фото, удаляем файл физически.
Год выпуска
Пробег
Цвет
Стоимость

При удалении Автомобиля, нужно также физически удалять фото. Не забыть что при удалении марки или модели, также надо удалить все связанные с ними авто, включая изображения.

Для списка авто нужно предусмотреть сортировку перетаскиванием. Такой режим есть в backpackforlaravel из коробки. Пример реализации сортировки - пакет crudmenu (https://github.com/Laravel-Backpack/MenuCRUD).

###Сайт
Выбрать любую готовую html-тему на свое усмотрение. 
Тема должна содержать плитку. 
Подойдет пример на bootstrap https://startbootstrap.com/templates/ecommerce/

Сайт - это всего 2 разных view.

Нужно вывести плитку автомобилей, добавленных в админке в соответствии с пользовательской сортировкой. Для каждой карточки сделать переход на подробную страницу.

Реализация сайта на Vue / Angular / React будет большим плюсом. 

####Результат работы
Результат работы нужно “выкатить” на тестовую площадку и предоставить ссылку. Если нужен хостинг, то по запросу предоставим площадку и доступ.

<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
